//
//  ViewController.h
//  ClientDemo
//
//  Created by zc on 13-8-16.
//  Copyright (c) 2013年 zc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncSocket.h"
@interface ViewController : UIViewController
@property (retain,nonatomic)AsyncSocket *client;
- (IBAction)bgTaped:(id)sender;

- (IBAction)ClientToSever:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *sendMSG;
- (IBAction)send:(id)sender;
@property (retain, nonatomic) IBOutlet UITextView *resiveMSG;

@end
